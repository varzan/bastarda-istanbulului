#!/bin/bash

for f in `ls sounds/*.mp3`
do
    cutf=${f/.mp3/.silent.mp3}
    sox $f $cutf silence 1 0.1 1%
    mv $cutf $f
done
