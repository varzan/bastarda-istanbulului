#!/bin/bash

for f in `ls sounds/*.mp3`
do
    cutf=${f/.mp3/.cut.mp3}
    sox $f $cutf trim 0 "-0:01.1"
    mv $cutf $f
done
